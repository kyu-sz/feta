class Tracker:
    def __init__(self, init_frame, init_bbox):
        raise NotImplementedError

    def track(self, frame):
        raise NotImplementedError
